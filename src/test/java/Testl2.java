import com.noise.Geron;
import org.junit.*;
import org.junit.rules.ExpectedException;


public class Testl2 {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void start() {
        System.out.println("Запуск тестов...");
    }
    @Test
    public void GeronTest() {
        new Geron(4);
        Assert.assertEquals(2, Geron.find(4),0);
        new Geron(16);
        Assert.assertEquals(4, Geron.find(4),0);
        new Geron(25);
        Assert.assertEquals(5, Geron.find(5),0);
        new Geron(30);
        Assert.assertEquals(5.477225575051661, Geron.find(6),0);
        new Geron(8000);
        Assert.assertEquals(89.44271909999159, Geron.find(900),0);
        new Geron(56325);
        Assert.assertEquals(237.32888572611637, Geron.find(240),0);
       }
    @Test
    public void GeronTestSecond() {
        new Geron(-4);
        Assert.assertEquals(4.80176078569419, Geron.find(4),0);
        new Geron(16);
        Assert.assertEquals(-4, Geron.find(-4),0);
        new Geron(25);
        Assert.assertEquals(-5, Geron.find(-5),0);
        new Geron(30);
        Assert.assertEquals(-5.477225575051661, Geron.find(-6),0);
        new Geron(8000);
        Assert.assertEquals(-89.44271909999159, Geron.find(-900),0);
        new Geron(56325);
        Assert.assertEquals(-237.32888572611637, Geron.find(-240),0);
    }

    @AfterClass
    public static void finish() {
        System.out.println("Тесты Завершенны...");
    }
}
