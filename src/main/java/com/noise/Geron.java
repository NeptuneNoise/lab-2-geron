package com.noise;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Geron {

    private static Scanner cont;
    private static double x = 0;

    public static void main (String[] args)throws IOException{
        while (true) {
            getGeron();
            System.out.println("\nХотите начать сначала? Введите: ''Yes''\nДля выхода нажмите любую клавишу");
            cont = new Scanner(System.in);
            String operation;
            operation = cont.next();
            if (operation.equals("Yes")) {
            }
            else {
                System.out.println("Работа завершена, Спасибо!");
                System.exit(0);
            }
        }
    }
    public Geron(double x){
        Geron.x = x;
    }
    public static void getGeron() throws IOException{
        BufferedReader key = new BufferedReader (new InputStreamReader(System.in));
        System.out.println("Введите число для расчета:");
        double y = 0;
        String f = key.readLine();
        try {
            x = Double.parseDouble(f);
        } catch (NumberFormatException e) {
            System.err.println("Ошибка ввода");
            System.out.println("Для рестарта введите: 'Yes' \nДля выхода нажмите любую клавишу");
            cont = new Scanner(System.in);
            String error;
            error = cont.next();
            if (error.equals("Yes")) {
                getGeron();//Рекурсия
            }
            else {
                System.out.println("Работа завершена, Спасибо!");
                System.exit(0);
            }

        }
        System.out.println("Введите свое предположение:");
        String d = key.readLine();
        try {
            y = Double.parseDouble(d);
        } catch (NumberFormatException e){
            System.err.println("Ошибка ввода");
            System.out.println("Для рестарта введите: 'Yes' \nДля выхода нажмите любую клавишу");
            cont = new Scanner(System.in);
            String error;
            error = cont.next();
            if (error.equals("Yes")) {
                getGeron();//Рекурсия
            }
            else {
                System.out.println("Работа завершена, Спасибо!");
                System.exit(0);
            }
        }
        System.out.printf("Ответ: "+find(y));
    }
    //Основной цикл расчета
    public static double find(double y){
        int i = 0;
        while ((y*y>x)&&(i<10)){
            y=(y+x/y)/2;
            i++;
        }
        return y;
    }
}
